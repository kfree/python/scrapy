import json
import os
import time
from datetime import datetime
from xml.etree import ElementTree

import requests


def create_sitemap(my_input_urls, my_page_number):
    file_name = 'bibliography_sitemap' + str(my_page_number) + '.xml'
    print(file_name)
    root = ElementTree.Element('urlset')
    root.attrib['xmlns:xsi'] = "http://www.w3.org/2001/XMLSchema-instance"
    root.attrib[
        'xsi:schemaLocation'] = "http://www.sitemaps.org/schemas/sitemap/0.9 " \
                                "http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd "
    root.attrib['xmlns'] = "http://www.sitemaps.org/schemas/sitemap/0.9"
    dt = datetime.now().strftime("%Y-%m-%d")
    for input_url in my_input_urls:
        doc = ElementTree.SubElement(root, "url")
        ElementTree.SubElement(doc, "loc").text = input_url
        ElementTree.SubElement(doc, "lastmod").text = dt
        ElementTree.SubElement(doc, "changefreq").text = "weekly"
        ElementTree.SubElement(doc, "priority").text = "0.8"
    tree = ElementTree.ElementTree(root)
    tree.write(file_or_filename=file_name, encoding='utf-8', xml_declaration=True)
    return True


try:
    os.remove('debug')
    os.remove('links')
    os.remove('work')
    for x in range(0, 100):
        filename = 'bibliography_sitemap' + str(x) + '.xml'
        print(filename)
        os.remove(filename)
except OSError:
    pass
baseUrl = "https://www.queenslibrary.org"
page_number = 1
item_number = 1
PAGE_LIMIT = 50000
ITEM_LOWER_LIMIT = 1
ITEM_UPPER_LIMIT = 2000000
input_urls = []
for x in range(ITEM_LOWER_LIMIT, ITEM_UPPER_LIMIT):
    try:
        payload = {'bib': str(x) + ',', 'queue': 'true'}
        time.sleep(0.1)
        r = requests.get('https://devapi.queenslibrary.org/bibs', params=payload)
        if r.status_code == 200:
            with open('debug', 'a') as f:
                f.write("\n")
                f.write("success!\n")
                f.write("\n")
                f.write(r.text)
                f.write("\n")
                f.close()
            data = json.loads(r.text)
            callUrl = data['_embedded']['bibs'][0]['callUrl']
            timeStamp = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(data['_embedded']['bibs'][0]['timestamp']))
            if "//" not in callUrl:
                with open('links', 'a') as f:
                    f.write("\n")
                    f.write(callUrl)
                    f.write("\n")
                    f.close()
                input_urls.append(baseUrl + callUrl)
                item_number += 1
            if item_number % PAGE_LIMIT == 0 or x == ITEM_UPPER_LIMIT - 1:
                with open('work', 'a') as f:
                    f.write("\n")
                    f.write(str(x))
                    f.close()
                data = json.loads(r.text)
                create_sitemap(input_urls, page_number)
                page_number += 1
                input_urls = []
        else:
            with open('debug', 'a') as f:
                f.write("\n")
                f.write("failure!\n")
                f.write("\n")
                f.write(r.text)
                f.write("\n")
                f.close()
    except:
        pass
